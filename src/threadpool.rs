use std::marker::PhantomData;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::thread::JoinHandle;

type Job<'a> = Box<dyn FnOnce() + Send + 'a>;

pub struct Worker {
    thread: Option<JoinHandle<()>>,
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    work_queue: mpsc::Sender<Message>,
}

enum Message {
    Join,
    Work(Job<'static>),
}

impl ThreadPool {
    pub fn new(num_threads: usize) -> Self {
        assert!(num_threads > 0);

        let mut workers = Vec::with_capacity(num_threads);
        let (tx, rx) = mpsc::channel();
        let rx = Arc::new(Mutex::new(rx));

        for _ in 0..num_threads {
            let rx = rx.clone();
            let thread = thread::spawn(move || loop {
                let msg = {
                    // Make sure we drop the lock as soon as possible
                    rx.lock().unwrap().recv()
                };
                match msg {
                    Ok(Message::Join) => break,
                    Ok(Message::Work(j)) => j(),
                    Err(_) => break,
                }
            });
            workers.push(Worker {
                thread: Some(thread),
            });
        }

        ThreadPool {
            work_queue: tx,
            workers,
        }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        self.work_queue
            .send(Message::Work(Box::new(f)))
            .expect("All ThreadPool threads have panicked.");
    }

    pub fn scoped<'env, E, R>(self, e: E) -> R
    where
        E: FnOnce(&Scope<'env>) -> R,
    {
        let scope = Scope {
            pool: self,
            scope: PhantomData,
        };
        e(&scope)
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in 0..self.workers.len() {
            self.work_queue.send(Message::Join).unwrap();
        }
        for worker in &mut self.workers {
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

pub struct Scope<'env> {
    pool: ThreadPool,
    // Choosing just `PhantomData<&'env ()>` would allow the compiler to infer a larger
    // lifetime than `'env`, because `PhantomData<&'env ()>` is covariant. But we want the compiler
    // to infer exactly `'env` because otherwise the inner closures could capture values of the outer
    // closure by reference. This is not possible if they have exactly lifetime `'env` because the outer
    // closure does not borrow from inside itself and thus the outer closure is not part of `'env`.
    scope: PhantomData<::std::cell::Cell<&'env ()>>,
}

impl<'env> Scope<'env> {
    /// The `ThreadPool` of this scope is getting dropped at the end of the `ThreadPool::scoped` call,
    /// resulting in all threads getting joined. It has to be dropped, because `ThreadPool::scoped` takes
    /// ownership of it, which means the `ThreadPool` could not have been passed to `std::mem::forget`.
    /// Any reference into the environment of the closure passed
    /// to the scoped call, which has lifetime `'env`, is such valid for the entire lifetime of the
    /// `ThreadPool`. We can safely transmute the `'env` lifetime to `'static`.
    pub fn execute<'inner, F>(&self, f: F)
    where
        F: FnOnce() + Send + 'inner,
        'inner: 'env,
    {
        let f = unsafe { std::mem::transmute::<Job<'env>, Job<'static>>(Box::new(f)) };
        self.pool.work_queue.send(Message::Work(f)).unwrap();
    }
}
