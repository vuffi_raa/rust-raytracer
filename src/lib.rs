use rand::prelude::*;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

pub mod color;
mod threadpool;

use crate::threadpool::ThreadPool;
pub use color::Color;

pub struct Settings {
    pub horizontal_resolution: usize,
    pub vertical_resolution: usize,
    pub aa_samples: u32,
    pub num_threads: usize,
}

pub struct Scene {
    pub objects: Vec<Shape>,
    pub camera: Camera,
}

impl Scene {
    pub fn render(&self, settings: &Settings) -> Vec<Color> {
        let Settings {
            horizontal_resolution,
            vertical_resolution,
            aa_samples,
            num_threads,
            ..
        } = *settings;

        let mut pixels = Vec::with_capacity(horizontal_resolution * vertical_resolution);
        pixels.resize(
            horizontal_resolution * vertical_resolution,
            Color(1.0, 1.0, 1.0),
        );
        let threadpool = ThreadPool::new(num_threads);

        threadpool.scoped(|s| {
            for (y, row) in pixels.chunks_exact_mut(horizontal_resolution).enumerate() {
                s.execute(move || {
                    let mut rng = rand::thread_rng();
                    for x in 0..horizontal_resolution {
                        let mut color = Color(0.0, 0.0, 0.0);
                        for _ in 0..aa_samples {
                            // TODO: Better sampling pattern
                            let u = (x as f64 + rng.gen::<f64>()) / (horizontal_resolution as f64);
                            let v = (y as f64 + rng.gen::<f64>()) / (vertical_resolution as f64);
                            let ray = self.camera.ray(u, v);
                            color += self.cast_ray(&ray, 0);
                        }
                        row[x] = (color / (aa_samples as f64)).gamma_corrected()
                    }
                })
            }
        });
        pixels
    }

    pub fn cast_ray(&self, ray: &Ray, depth: i32) -> Color {
        if let Some(intersection) = closest_intersection(ray, &self.objects) {
            if depth > 20 {
                return Color(0.0, 0.0, 0.0);
            }
            let material = &intersection.material;
            if let Some(scatter) = material.scatter(ray, &intersection) {
                scatter.attenuation * self.cast_ray(&scatter.ray, depth + 1)
            } else {
                Color(0.0, 0.0, 0.0)
            }
        } else {
            let t = 0.5 * (ray.dir.unit().2 + 1.0);
            (1.0 - t) * Color(1.0, 1.0, 1.0) + t * Color(0.5, 0.7, 1.0)
        }
    }
}

fn random_in_unit_sphere() -> V3 {
    let mut rng = rand::thread_rng();
    let mut result = V3(rng.gen(), rng.gen(), rng.gen());
    while result.norm() >= 1.0 {
        result = V3(rng.gen(), rng.gen(), rng.gen());
    }
    result
}

pub struct ScatterResult {
    ray: Ray,
    attenuation: Color,
}

#[derive(Debug, Clone)]
pub enum Material {
    Lambertian {
        attenuation: Color,
    },
    Metal {
        attenuation: Color,
        fuzziness: f64,
    },
    Dielectric {
        attenuation: Color,
        refractive_index: f64,
    },
}

impl Material {
    pub fn scatter(&self, ray: &Ray, intersection: &Intersection) -> Option<ScatterResult> {
        match *self {
            Material::Lambertian { attenuation } => {
                let target =
                    intersection.point + intersection.normal.unit() + random_in_unit_sphere();
                Some(ScatterResult {
                    ray: Ray::from_to(intersection.point, target),
                    attenuation,
                })
            }
            Material::Metal {
                attenuation,
                fuzziness,
            } => {
                let reflected = ray.dir.unit().reflect_along_normal(intersection.normal);
                let scattered = Ray {
                    origin: intersection.point,
                    dir: reflected + fuzziness * random_in_unit_sphere(),
                };
                if scattered.dir.dot(intersection.normal) > 0.0 {
                    Some(ScatterResult {
                        ray: scattered,
                        attenuation,
                    })
                } else {
                    None
                }
            }
            Material::Dielectric {
                attenuation,
                refractive_index,
            } => {
                let i = ray.dir;
                let (n, eta_i, eta_t) = if i.dot(intersection.normal) <= 0.0 {
                    // v and n point in different directions, we are entering the material
                    // NOTE: We are assuming an air to material transition. We do currently not handle
                    // material to material transitions.
                    (intersection.normal, 1.0, refractive_index)
                } else {
                    // We are leaving the material
                    (-intersection.normal, refractive_index, 1.0)
                };
                let eta = eta_i / eta_t;
                let reflection_prob = fresnel(ray.dir, intersection.normal, eta_i, eta_t);
                if let Some(refracted) = ray.refract(intersection.point, n, eta) {
                    let rand: f64 = rand::thread_rng().gen();
                    if rand <= reflection_prob {
                        Some(ScatterResult {
                            ray: ray.reflect(intersection.point, n),
                            attenuation,
                        })
                    } else {
                        Some(ScatterResult {
                            ray: refracted,
                            attenuation,
                        })
                    }
                } else {
                    Some(ScatterResult {
                        ray: ray.reflect(intersection.point, n),
                        attenuation,
                    })
                }
            }
        }
    }
}

// Implementation taken from https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
fn fresnel(i: V3, normal: V3, eta_i: f64, eta_t: f64) -> f64 {
    let cos_theta_i = i.unit().dot(normal.unit());
    // Via Snell's Law
    let sin_theta_t = eta_i / eta_t * (1.0 - cos_theta_i.powf(2.0)).max(0.0).sqrt();
    if sin_theta_t >= 1.0 {
        // Total internal reflection, because determinant in refract would be < 0.0
        // Reflect all light
        1.0
    } else {
        let cos_theta_t = (1.0 - sin_theta_t.powf(2.0)).max(0.0).sqrt();
        // NOTE: No clue why we are absing this value. But is is required.
        // Probably because we are assuming that the incident angle is below 90 degrees?
        let cos_theta_i = cos_theta_i.abs();
        let rs = ((eta_t * cos_theta_i) - (eta_i * cos_theta_t))
            / ((eta_t * cos_theta_i) + (eta_i * cos_theta_t));
        let rt = ((eta_i * cos_theta_i) - (eta_t * cos_theta_t))
            / ((eta_i * cos_theta_i) + (eta_t * cos_theta_t));
        (rs.powf(2.0) + rt.powf(2.0)) / 2.0
    }
}

/// Returns the refracted direction of v along the normal given the ratio of refractive indices.
/// Returns None if no refraction is possible.
/// See https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
/// for derivation
fn refract(i: V3, normal: V3, eta: f64) -> Option<V3> {
    let cos_theta = i.unit().dot(normal.unit()); // a.dot(b) = |a||b|cos(theta) = cos(theta)
    let discriminant = 1.0 - eta.powf(2.0) * cos_theta.powf(2.0);
    if discriminant < 0.0 {
        None
    } else {
        Some(eta * (i + cos_theta * normal) - normal * discriminant.sqrt())
    }
}

pub enum Shape {
    Sphere {
        center: V3,
        radius: f64,
        material: Material,
    },
}

#[derive(Debug)]
pub struct Ray {
    pub origin: V3,
    pub dir: V3,
}

impl Ray {
    pub fn reflect(&self, p: V3, n: V3) -> Ray {
        Ray {
            origin: p,
            dir: self.dir.reflect_along_normal(n),
        }
    }

    pub fn refract(&self, p: V3, n: V3, refractive_index_ratio: f64) -> Option<Ray> {
        if let Some(dir) = refract(self.dir, n, refractive_index_ratio) {
            Some(Ray { origin: p, dir })
        } else {
            None
        }
    }

    pub fn from_to(origin: V3, target: V3) -> Ray {
        Ray {
            origin,
            dir: target - origin,
        }
    }
    pub fn at(&self, t: f64) -> V3 {
        let Ray { origin, dir } = *self;
        origin + t * dir
    }
}

#[derive(Debug)]
pub struct Intersection {
    pub param: f64,
    pub point: V3,
    pub normal: V3,
    pub material: Material,
}

pub fn closest_intersection(ray: &Ray, shapes: &[Shape]) -> Option<Intersection> {
    shapes
        .iter()
        .flat_map(|s| intersect(ray, s))
        .min_by(|a, b| a.param.partial_cmp(&b.param).expect("Ray position is NaN"))
}

pub fn intersect(ray: &Ray, shape: &Shape) -> Option<Intersection> {
    let Ray { origin, dir } = *ray;
    match *shape {
        Shape::Sphere {
            center,
            radius,
            ref material,
            ..
        } => {
            // A sphere at position c with radius r consists of points p with <p - c, p - c> = r^2,
            // where <., .> is the standard dot product.
            // We are thus checking <r(t) - c, r(t) - c> = r^2, where r(t) is the ray at position t
            // This is equal to:
            // <r(t) - c, r(t) - c> - r^2 = 0
            // => <o + td - c, o + td - c> - r^2 = 0
            // =>t^2 <d, d> + 2t <d, o - c> + <o - c, o - c> - r^2 = 0

            // This formula can be solved via x = -b +- sqrt(b^2 - 4ac) / 2a
            // We can then determine the number of intersections via the determinant b^2 - 4ac
            // determinant > 0: 2 intersections
            // determinant = 0: 1 intersection
            // determinant < 0: 0 intersections
            let a = dir.dot(dir);
            let b = 2.0 * dir.dot(origin - center);
            let c = (origin - center).dot(origin - center) - radius.powf(2.0);
            let determinant = b.powf(2.0) - 4.0 * a * c;

            if determinant > 0.0 {
                let closest_intersection = (-b - f64::sqrt(determinant)) / (2.0 * a);
                if closest_intersection > 0.001 {
                    let point = ray.at(closest_intersection);
                    let normal = point - center;
                    return Some(Intersection {
                        param: closest_intersection,
                        material: material.clone(),
                        point,
                        normal,
                    });
                }
            }
            None
        }
    }
}

#[derive(Debug)]
pub struct Camera {
    pub position: V3,
    pub viewport_origin: V3,
    pub viewport_horizontal: V3,
    pub viewport_vertical: V3,
}

impl Camera {
    pub fn new(position: V3, look_at: V3, up: V3, vertical_fov: f64, aspect_ratio: f64) -> Camera {
        let forward = (look_at - position).unit();
        let right = forward.cross(up).unit();
        let up = right.cross(forward);

        // tan(x) = opposite/adjacent
        // Our viewing plane ist 1 unit away from the camera, leaving us with
        // tan(x) = opposite for the upper half of the viewport
        let h = f64::tan(vertical_fov.to_radians() / 2.0);
        let viewport_height = 2.0 * h;
        let viewport_width = viewport_height * aspect_ratio;

        let viewport_vertical = viewport_height * up;
        let viewport_horizontal = viewport_width * right;
        let viewport_origin =
            position + forward - viewport_horizontal / 2.0 + viewport_vertical / 2.0;

        Camera {
            position,
            viewport_origin,
            viewport_horizontal,
            viewport_vertical,
        }
    }

    pub fn ray(&self, u: f64, v: f64) -> Ray {
        Ray {
            origin: self.position,
            dir: self.viewport_origin + u * self.viewport_horizontal
                - v * self.viewport_vertical
                - self.position,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct V3(pub f64, pub f64, pub f64);

impl V3 {
    pub fn dot(self, rhs: Self) -> f64 {
        self.0 * rhs.0 + self.1 * rhs.1 + self.2 * rhs.2
    }

    pub fn norm(self) -> f64 {
        f64::sqrt(self.0.powf(2.0) + self.1.powf(2.0) + self.2.powf(2.0))
    }

    pub fn unit(self) -> Self {
        self / self.norm()
    }

    pub fn cross(self, rhs: V3) -> V3 {
        V3(
            self.1 * rhs.2 - self.2 * rhs.1,
            self.2 * rhs.0 - self.0 * rhs.2,
            self.0 * rhs.1 - self.1 * rhs.0,
        )
    }

    pub fn reflect_along_normal(self, n: V3) -> V3 {
        self - 2.0 * self.dot(n) * n
    }
}

impl Add for V3 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        V3(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
    }
}

impl AddAssign for V3 {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs
    }
}

impl Sub for V3 {
    type Output = V3;

    fn sub(self, rhs: Self) -> Self::Output {
        V3(self.0 - rhs.0, self.1 - rhs.1, self.2 - rhs.2)
    }
}

impl Neg for V3 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        V3(-self.0, -self.1, -self.2)
    }
}

impl SubAssign for V3 {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs
    }
}

impl Mul<f64> for V3 {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self::Output {
        V3(rhs * self.0, rhs * self.1, rhs * self.2)
    }
}

impl MulAssign<f64> for V3 {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs
    }
}

impl Mul<V3> for f64 {
    type Output = V3;

    fn mul(self, rhs: V3) -> Self::Output {
        V3(self * rhs.0, self * rhs.1, self * rhs.2)
    }
}

impl Div<f64> for V3 {
    type Output = V3;

    fn div(self, rhs: f64) -> Self::Output {
        V3(self.0 / rhs, self.1 / rhs, self.2 / rhs)
    }
}

impl DivAssign<f64> for V3 {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs
    }
}

#[cfg(test)]
mod tests {
    use crate::{intersect, Ray, Shape, V3};

    #[test]
    fn vector_ops() {
        let zero = V3(0.0, 0.0, 0.0);
        let two = V3(2.0, 2.0, 2.0);

        let mut a = V3(1.0, 1.0, 1.0);
        assert_eq!(two, a + a);
        a += a;
        assert_eq!(two, a);

        let mut a = V3(1.0, 1.0, 1.0);
        assert_eq!(zero, a - a);
        a -= a;
        assert_eq!(zero, a);

        let mut a = V3(1.0, 1.0, 1.0);
        assert_eq!(two, a * 2.0);
        assert_eq!(two, 2.0 * a);
        a *= 2.0;
        assert_eq!(two, a);

        let mut a = V3(1.0, 1.0, 1.0);
        assert_eq!(two, a / 0.5);
        a /= 0.5;
        assert_eq!(two, a);
    }

    #[test]
    fn intersect_test() {
        let ray = Ray {
            origin: V3(0.0, 0.0, 0.0),
            dir: V3(1.0, 0.0, 0.0),
        };
        let sphere = Shape::Sphere {
            center: V3(4.0, 0.0, 0.0),
            radius: 1.0,
            color: V3(1.0, 0.0, 0.0),
        };

        let intersection = intersect(&ray, &sphere);
        assert!(intersection.is_some());
        let intersection = intersection.unwrap();
        assert_eq!(intersection.point, V3(3.0, 0.0, 0.0));
        assert_eq!(intersection.normal, V3(-1.0, 0.0, 0.0));

        let ray = Ray {
            origin: V3(0.0, 0.0, 0.0),
            dir: V3(0.0, 1.0, 0.0),
        };
        let intersection = intersect(&ray, &sphere);
        assert!(intersection.is_none());
    }
}
