use pixels::{Pixels, SurfaceTexture};
use raytracer::{Camera, Color, Material, Scene, Settings, Shape, V3};
use std::error::Error;
use std::fs::File;
use std::io::BufWriter;
use std::path::PathBuf;
use std::time::Instant;
use winit::dpi::LogicalSize;
use winit::event::Event;
use winit::event::WindowEvent;
use winit::event::WindowEvent::Resized;
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;

#[cfg(not(debug_assertions))]
const WIDTH: usize = 1920;
#[cfg(not(debug_assertions))]
const HEIGHT: usize = 1080;
#[cfg(not(debug_assertions))]
const AA_SAMPLES: u32 = 200;

#[cfg(debug_assertions)]
const WIDTH: usize = 1920 / 4;
#[cfg(debug_assertions)]
const HEIGHT: usize = 1080 / 4;
#[cfg(debug_assertions)]
const AA_SAMPLES: u32 = 50;

const NUM_THREADS: usize = 8;

fn main() -> Result<(), Box<dyn Error>> {
    let aspect_ratio = (WIDTH as f64) / (HEIGHT as f64);
    let camera = Camera::new(
        V3(-10.0, 0.0, 0.0),
        V3(1.0, 0.0, 0.0),
        V3(0.0, 0.0, 1.0),
        90.0,
        aspect_ratio,
    );

    /*let objects = vec![
        Shape::Sphere {
            center: V3(4.0, 0.0, 0.),
            radius: 1.0,
            material: Material::Lambertian { attenuation: Color(0.5, 0.0, 0.0) },
        },
        Shape::Sphere {
            center: V3(4.0, 0.0, -5.0),
            radius: 4.0,
            material: Material::Lambertian { attenuation: Color(0.5, 0.5, 0.5)},
        },
        Shape::Sphere {
            center: V3(0.0, 0.0, 0.0),
            radius: 5.0,
            material: Material::Dielectric { attenuation: Color(1.0, 1.0, 1.0), refractive_index: 1.5 },
        },
        Shape::Sphere {
            center: V3(-4.0, 8.0, 0.0),
            radius: 5.0,
            material: Material::Lambertian { attenuation: Color( 0.5, 0.2, 0.2 )},
        },
        Shape::Sphere {
            center: V3(7.0, 2.0, -1.0),
            radius: 3.0,
            material: Material::Lambertian { attenuation: Color(0.2, 0.5, 0.1) },
        }
    ]; */

    let scene = Scene {
        objects: vec![
            Shape::Sphere {
                center: V3(0.0, 0.0, -105.0),
                radius: 100.0,
                material: Material::Lambertian {
                    attenuation: Color(0.5, 0.5, 0.5),
                },
            },
            Shape::Sphere {
                center: V3(0.0, 0.0, 0.0),
                radius: 5.0,
                material: Material::Dielectric {
                    attenuation: Color(1.0, 1.0, 1.0),
                    refractive_index: 1.5,
                },
            },
            Shape::Sphere {
                center: V3(-20.0, 0.0, 6.0),
                radius: 3.0,
                material: Material::Lambertian {
                    attenuation: Color(1.0, 0.0, 0.0),
                },
            },
            Shape::Sphere {
                center: V3(-20.0, 0.0, 0.0),
                radius: 3.0,
                material: Material::Lambertian {
                    attenuation: Color(0.0, 1.0, 0.0),
                },
            },
            Shape::Sphere {
                center: V3(-20.0, 0.0, -6.0),
                radius: 3.0,
                material: Material::Lambertian {
                    attenuation: Color(0.0, 0.0, 1.0),
                },
            },
        ],
        camera,
    };

    let settings = Settings {
        horizontal_resolution: WIDTH,
        vertical_resolution: HEIGHT,
        aa_samples: AA_SAMPLES,
        num_threads: NUM_THREADS,
    };

    println!("Raytracing...");
    let begin = Instant::now();
    let colors = scene.render(&settings);
    let end = Instant::now();
    let elapsed = end.duration_since(begin);
    let minutes = elapsed.as_secs() / 60;
    let seconds = elapsed.as_secs() % 60;
    println!("Done! Time elapsed: {}m{}s (wall-clock)", minutes, seconds);

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(LogicalSize::new(WIDTH as f64, HEIGHT as f64))
        .with_title("Raytracer")
        .build(&event_loop)
        .expect("Could not create window");

    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(WIDTH as u32, HEIGHT as u32, surface_texture)?
    };

    let mut rgba = Vec::with_capacity(colors.len() * 4);
    for color in colors.into_iter() {
        rgba.extend_from_slice(&color.as_rgba_bytes());
    }
    let rgba = rgba;

    // Write render result to window
    let framebuffer = pixels.get_frame();
    framebuffer.copy_from_slice(&rgba);

    // Write render result to file
    let args: Vec<String> = std::env::args().collect();
    if args.len() >= 2 {
        let path = PathBuf::from(&args[1]);
        println!("Writing png to: {}", path.display());
        let file = File::create(path)?;
        let w = BufWriter::new(file);
        let mut encoder = png::Encoder::new(w, WIDTH as u32, HEIGHT as u32);
        encoder.set_color(png::ColorType::RGBA);
        encoder.set_depth(png::BitDepth::Eight);
        let mut writer = encoder.write_header()?;
        writer.write_image_data(&rgba)?;
    }

    // Run window
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => *control_flow = ControlFlow::Exit,
            Event::WindowEvent {
                event: Resized(size),
                ..
            } => {
                pixels.resize(size.width, size.height);
            }
            Event::RedrawRequested(_) => {
                if pixels
                    .render()
                    .map_err(|e| eprintln!("pixels.render() failed: {:?}", e))
                    .is_err()
                {
                    *control_flow = ControlFlow::Exit;
                    return;
                }
            }
            _ => (),
        }
    })
}
